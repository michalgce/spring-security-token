package pl.witkowski;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class TokenController {

    @GetMapping(path = "/open")
    public String openMethod() {
        return "this is unsecured method";
    }

    @GetMapping(path = "/secret")
    public String secretMethod(@AuthenticationPrincipal UserWithToken userWithToken) {
        return "Wow you somehow point to my secret with token " + userWithToken.getToken();
    }
}
