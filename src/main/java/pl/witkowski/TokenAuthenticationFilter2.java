package pl.witkowski;

import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class TokenAuthenticationFilter2 extends OncePerRequestFilter {

    private static final String SECURITY_TOKEN_HEADER = "X-Token";

    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain) throws ServletException, IOException {
        final String token = request.getHeader( SECURITY_TOKEN_HEADER );

        if (token != null) {

            List<GrantedAuthority> grantedAuths = com.sun.tools.javac.util.List.of(new SimpleGrantedAuthority("USER"));

            UserWithToken userWithToken = new UserWithToken("test", "test", true, true, true,
                    true, grantedAuths);
            userWithToken.setToken(token);

            Authentication auth = new UsernamePasswordAuthenticationToken(userWithToken, userWithToken.getPassword(), grantedAuths);
            SecurityContextHolder.getContext().setAuthentication(auth);
        } else {
            SecurityContextHolder.getContext().setAuthentication(null);
        }

        filterChain.doFilter(request, response);
    }
}
