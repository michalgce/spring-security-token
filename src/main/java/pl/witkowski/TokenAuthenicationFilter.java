package pl.witkowski;

import com.sun.tools.javac.util.List;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TokenAuthenicationFilter extends AbstractAuthenticationProcessingFilter {

    private static final String SECURITY_TOKEN_HEADER = "X-Token";

    protected TokenAuthenicationFilter() {
        super("/*");
    }

    @Override
    public Authentication attemptAuthentication(final HttpServletRequest request, final HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        final String token = request.getHeader( SECURITY_TOKEN_HEADER );
        if (token != null) {
            SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority("USER");
            UserWithToken userWithToken = new UserWithToken("test", "test", true, true, true,
                    true, List.of(simpleGrantedAuthority));
            userWithToken.setToken(token);

            return new AnonymousAuthenticationToken("DUPA", userWithToken, List.of(simpleGrantedAuthority));
        } else {
            throw new InsufficientAuthenticationException("This guy is weird! KICK HIM OFF!!");
        }

    }
}
